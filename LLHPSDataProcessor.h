#ifndef __LLHPSDATAPROCESSOR_H_
#define __LLHPSDATAPROCESSOR_H_

#include <QListView>
#include <QModelIndex>
#include <QtCharts>
#include <QChartView>

#include "./LCDDevice.h"

using namespace QtCharts;

/// The maximum values on the X axis.
#define CHART_MAX_VALUES 150

/**
 * @brief The LLHPSDataProcessor class
 *
 * This class is responsible for processing the data as received by the Bluetooth device.
 */
class LLHPSDataProcessor : public QThread
{
    Q_OBJECT
private:
    /// The value to be processed on a seperate thread.
    int chartVal;
    /// The LCD device.
    LCDDevice * _lcdModel;
public:
    // The series which will contain the new value.
    QVector<QPointF> _serie2;
private:
    void run();
public:
    /**
     * @brief The constructor of the LLHPSDataProcessor class.
     * @param parent The parent.
     */
    inline explicit LLHPSDataProcessor(QWidget * parent = 0) : QThread(parent), _lcdModel(new LCDDevice) {}
    /**
     * @brief Add a point to proces.
     * @param val The point to proces.
     */
    inline void AddPoint(uint16_t val) {chartVal = val;}
    /**
     * @brief Get the processed series.
     * @return The processed series.
     */
    inline QVector<QPointF>* GetSeries() {return &_serie2;}
};

#endif // __LLHPSDATAPROCESSOR_H_
