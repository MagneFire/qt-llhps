#ifndef CHARACTERISTICINFOMODEL_H
#define CHARACTERISTICINFOMODEL_H


#include <QAbstractListModel>
#include <QObject>
#include <QStringList>

#include "Bluetooth/characteristicinfo.h"


class CharacteristicInfoModel : public QAbstractListModel
{
    Q_OBJECT

public:
    CharacteristicInfoModel(QList<QObject*> *strings, QObject *parent = 0)
        : QAbstractListModel(parent), deviceList(strings) {}

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,

                        int role = Qt::DisplayRole) const;



    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value,

                 int role = Qt::EditRole);



    bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
    bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());



private:
    QList<QObject*>* deviceList;
};


#endif // DEVICEINFOLISTMODEL_H
