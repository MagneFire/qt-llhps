#include "CharacteristicInfoModel.h"

int CharacteristicInfoModel::rowCount(const QModelIndex &) const
{
    return deviceList->count();
}


/*!
    Returns an appropriate value for the requested data.
    If the view requests an invalid index, an invalid variant is returned.
    Any valid index that corresponds to a string in the list causes that
    string to be returned.
*/


QVariant CharacteristicInfoModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= deviceList->size())
        return QVariant();

    if (role == Qt::DisplayRole)
        return ((CharacteristicInfo*)deviceList->at(index.row()))->getName();
    else if (role == Qt::UserRole)
        return QVariant::fromValue((CharacteristicInfo*)deviceList->at(index.row()));

    return QVariant();
}


/*!
    Returns the appropriate header string depending on the orientation of
    the header and the section. If anything other than the display role is
    requested, we return an invalid variant.
*/


QVariant CharacteristicInfoModel::headerData(int section, Qt::Orientation orientation,
                                     int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
        return QString("Column %1").arg(section);
    else
        return QString("Row %1").arg(section);
}


/*!
    Returns an appropriate value for the item's flags. Valid items are
    enabled, selectable, and editable.
*/


Qt::ItemFlags CharacteristicInfoModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}


/*!
    Changes an item in the string list, but only if the following conditions
    are met:

    * The index supplied is valid.
    * The index corresponds to an item to be shown in a view.
    * The role associated with editing text is specified.

    The dataChanged() signal is emitted if the item is changed.
*/


bool CharacteristicInfoModel::setData(const QModelIndex &index,
                              const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {

        deviceList->replace(index.row(), (CharacteristicInfo*)value.value<QObject*>());
        emit dataChanged(index, index);
        return true;
    }

    return false;
}


/*!
    Inserts a number of rows into the model at the specified position.
*/


bool CharacteristicInfoModel::insertRows(int position, int rows, const QModelIndex &)
{
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row) {
        //deviceList.insert(position, "");
    }

    endInsertRows();
    return true;

}


/*!
    Removes a number of rows from the model at the specified position.
*/


bool CharacteristicInfoModel::removeRows(int position, int rows, const QModelIndex &)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row) {
        deviceList->removeAt(position);
    }

    endRemoveRows();
    return true;

}
