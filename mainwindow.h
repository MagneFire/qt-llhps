#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QVBoxLayout>
#include <QLabel>
#include <QListView>
#include <QtCharts>

#include "Components/CollapsePanel.h"
#include "Components/StatusButton.h"
#include "Components/CollapseListView.h"

#include "./LLHPSDataProcessor.h"

#include "Bluetooth/device.h"
#include "Models/DeviceInfoListModel.h"
#include "Models/ServiceInfoListModel.h"
#include "Models/CharacteristicInfoModel.h"
#include "./LCDDevice.h"

using namespace QtCharts;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void clicked();
    void deviceConnectError(QLowEnergyController::Error);
    void deviceDisconnected();

    void serviceAdded();

    void characteristicsUpdated();

    void serviceNotification(QLowEnergyCharacteristic, QByteArray);

    void updateChart();

private:
    typedef enum _BluetoothStatuses
    {
        BLUETOOTH_STATUS_CONNECT_DEVICE,
        BLUETOOTH_STATUS_CONNECTING_DEVICE,
        BLUETOOTH_STATUS_CONNECT_SERVICE,
        BLUETOOTH_STATUS_CONNECTING_SERVICE,
        BLUETOOTH_STATUS_CONNECTED_SERVICE,
        BLUETOOTH_STATUS_CONNECT_ERROR
    } BluetoothStatuses;

    Ui::MainWindow *ui;
    Device * _bluetooth;
    BluetoothStatuses _bluetoothStatus;

    LLHPSDataProcessor * _llhpsDataProcessor;

    QChart *chart;
    QLineSeries * series;


    CollapseListView * _blDevicesView;

    CollapseListView * _blServicesView;

    LCDDevice * _lcdModel;

    QTimer * _timer;
};

#endif // MAINWINDOW_H
