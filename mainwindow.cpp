#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <chrono>

#define CHART_MAX_Y 1024

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _bluetooth(new Device()),
    _bluetoothStatus(BLUETOOTH_STATUS_CONNECT_DEVICE),
    _llhpsDataProcessor(new LLHPSDataProcessor()),
    chart(new QChart),
    series(new QLineSeries),
    _lcdModel(new LCDDevice()),
    _timer(new QTimer())
{
    // Setup UI.
    ui->setupUi(this);

    // Set interval to 5Hz.
    _timer->setInterval(1 / 5 * 1000);
    // Enable repeating timer.
    _timer->setSingleShot(false);
    // Connect timeout to updateChart function.
    connect(_timer, &QTimer::timeout, this, &MainWindow::updateChart);

    // Set random address, needed for the NRF51822.
    _bluetooth->setRandomAddress(true);

    // Restart scanning when scanning completes.
    connect(_bluetooth, &Device::deviceScanDone, _bluetooth, &Device::startDeviceDiscovery);
    // Connect the Bluetooth deviceConnectError signal to a slot in this class.
    connect(_bluetooth, &Device::deviceConnectError, this, &MainWindow::deviceConnectError);
    // Connect the Bluetooth deviceDisconnected signal to a slot in this class.
    connect(_bluetooth, &Device::deviceDisconnected, this, &MainWindow::deviceDisconnected);
    // Connect the Bluetooth serviceAdded signal to a slot in this class.
    connect(_bluetooth, &Device::serviceAdded, this, &MainWindow::serviceAdded);
    // Connect the Bluetooth characteristicsUpdated signal to a slot in this class.
    connect(_bluetooth, &Device::characteristicsUpdated, this, &MainWindow::characteristicsUpdated);
    // Connect the connect button clicked signal to a slot in this class.
    connect(ui->connectbtn, &QPushButton::clicked, this, &MainWindow::clicked);

    // Enable OpenGL acceleration.
    series->setUseOpenGL(true);

    // Hide legend.
    chart->legend()->hide();
    // Add series to chart.
    chart->addSeries(series);
    // Create default axis.
    chart->createDefaultAxes();
    // Set vertical range.
    chart->axisY()->setRange(0, 1024);
    // Set the chart title.
    chart->setTitle("Potentiometer analog reading history");
    // Create a new chartview.
    QChartView * chartView = new QChartView(chart);

    // Put the chart to the right of the Bluetooth settings panel.
    ui->gridLayout->addWidget(chartView, 0, 2, 1, 1);

    // Set the services view.
    _blDevicesView = new CollapseListView("Connect to a device");
    // Connect the double click signal to the service clicked slot.
    connect(_blDevicesView, &CollapseListView::ItemDoubleClicked, this, &MainWindow::clicked);
    // Create a new model, with a reference to the BLE services list. use that in the services view.
    _blDevicesView->SetModel(new DeviceInfoListModel(_bluetooth->getDevices()));
    // Update the devices view when a device is added.
    connect(_bluetooth, &Device::deviceAdded, _blDevicesView, &CollapseListView::update);
    // Expand de devices view.
    _blDevicesView->Expand();
    // Add services view to the ui.
    ui->bluetoothLayout->insertWidget(0, _blDevicesView);

    // Set the services view.
    _blServicesView = new CollapseListView("Connect to a service");
    // Disable the services view by default.
    _blServicesView->Disable();
    // Connect the double click signal to the service clicked slot.
    connect(_blServicesView, &CollapseListView::ItemDoubleClicked, this, &MainWindow::clicked);
    // Create a new model, with a reference to the BLE services list. use that in the services view.
    _blServicesView->SetModel(new ServiceInfoListModel(_bluetooth->getServices()));
    // Add services view to the ui.
    ui->bluetoothLayout->insertWidget(1, _blServicesView);

    // Call the device disconnected function. This makes sure that everything gets setup properly.
    deviceDisconnected();
}

MainWindow::~MainWindow()
{
    // Delete the UI.
    delete ui;
}


void MainWindow::clicked()
{
    // A pointer to the currently connected device.
    DeviceInfo* device;
    // A pointer to the currently connected service.
    ServiceInfo* service;
    // Check what status Bluetooth is in.
    switch(_bluetoothStatus)
    {
    // Check if there is an error or if we should connect.
    // In both cases, just allow the user to connect to a device.
    case BLUETOOTH_STATUS_CONNECT_ERROR:
    case BLUETOOTH_STATUS_CONNECT_DEVICE:
        // Get currently selected device.
        device = _blDevicesView->currentIndex().data(Qt::UserRole).value<DeviceInfo*>();
        // Check if the user selected an item.
        if (device == NULL) return;
        // Collapse device panel.
        _blDevicesView->Collapse();
        // Set device status button to loading.
        _blDevicesView->SetStatus(StatusButton::STATUS_LOADING);
        // Print some information.
        _blDevicesView->SetTitle("Connecting to "+device->getName());
        // Disable the connect button.
        ui->connectbtn->setEnabled(false);
        // Update the Bluetooth status.
        _bluetoothStatus = BLUETOOTH_STATUS_CONNECTING_DEVICE;
        // Connect to the device and get services.
        _bluetooth->scanServices(device->getAddress());
        break;
    // Check if a connection should be made to a service.
    case BLUETOOTH_STATUS_CONNECT_SERVICE:
        // Get currently selected service.
        service = _blServicesView->currentIndex().data(Qt::UserRole).value<ServiceInfo*>();
        // Check if the user selected an item.
        if (service == NULL) return;
        // Collapse the service panel.
        _blServicesView->Collapse();
        // Set service status button to loading.
        _blServicesView->SetStatus(StatusButton::STATUS_LOADING);
        // Print service name.
        _blServicesView->SetTitle("Connecting to "+service->getName());
        // Disable the connect button.
        ui->connectbtn->setEnabled(false);
        // Update the Bluetooth status.
        _bluetoothStatus = BLUETOOTH_STATUS_CONNECTING_SERVICE;
        // Connect to service and get characteristics.
        _bluetooth->connectToService(service->getUuid());
        break;
    // Check if a connection is already made to a service.
    case BLUETOOTH_STATUS_CONNECTED_SERVICE:
        // Disconnect from device.
        _bluetooth->disconnectFromDevice();
        break;
    // Handle other cases.
    default:
        break;
    }
}

void MainWindow::deviceConnectError(QLowEnergyController::Error)
{
    // Set the Bluetooth status to status error.
    _bluetoothStatus = BLUETOOTH_STATUS_CONNECT_ERROR;
    // Stop the timer there is not data to update the chart with.
    _timer->stop();
    // Set error indicator.
    _blDevicesView->SetStatus(StatusButton::STATUS_ERROR);
    // Show error message.
    _blDevicesView->SetTitle("Error while connecting to "+_bluetooth->getCurrentDevice()->getName());
    // Expand the devices list view.
    _blDevicesView->Expand();
    // Enable devices button.
    _blDevicesView->Enable();

    // Collapse services panel, if expanded.
    _blServicesView->Collapse();
    // Disable services button, if enabled.
    _blServicesView->Disable();
    // Reset the services view title.
    _blServicesView->SetTitle("Connect to a service");
    // Reset the services view status.
    _blServicesView->SetStatus(StatusButton::STATUS_NO_STATUS);
    // Reset the connect button text.
    ui->connectbtn->setText("Connect");
    // Enable the connect button.
    ui->connectbtn->setEnabled(true);
    // Restart device discovery.
    _bluetooth->startDeviceDiscovery();
}

void MainWindow::deviceDisconnected()
{
    // Check if there was an error prior to this function call.
    if (_bluetoothStatus == BLUETOOTH_STATUS_CONNECT_ERROR) return;
    // Reset the Bluetooth status.
    _bluetoothStatus = BLUETOOTH_STATUS_CONNECT_DEVICE;
    // Stop the timer there is not data to update the chart with.
    _timer->stop();
    // Set error indicator.
    _blDevicesView->SetStatus(StatusButton::STATUS_NO_STATUS);
    // Show error message.
    _blDevicesView->SetTitle("Connecting to a device");
    // Expand the devices list view.
    _blDevicesView->Expand();
    // Enable devices button.
    _blDevicesView->Enable();

    // Collapse services panel, if expanded.
    _blServicesView->Collapse();
    // Disable services button, if enabled.
    _blServicesView->Disable();
    // Reset the services view title.
    _blServicesView->SetTitle("Connect to a service");
    // Reset the services view status.
    _blServicesView->SetStatus(StatusButton::STATUS_NO_STATUS);
    // Set the connect button.
    ui->connectbtn->setText("Connect");
    // Enable the connect button.
    ui->connectbtn->setEnabled(true);
    // Restart device discovery.
    _bluetooth->startDeviceDiscovery();
}

void MainWindow::serviceAdded()
{
    // Get the current device.
    DeviceInfo * currentDevice = _bluetooth->getCurrentDevice();
    // Check if the connection to the device was succesful.
    if (_bluetooth->getServices()->size() > 0)
    {
        _bluetoothStatus = BLUETOOTH_STATUS_CONNECT_SERVICE;
        // Set devices view status to status OK.
        _blDevicesView->SetStatus(StatusButton::STATUS_OK);
        // Set devices view title.
        _blDevicesView->SetTitle("Connected to "+currentDevice->getName());
        // Disable the devices view.
        _blDevicesView->Disable();

        // Enable the services view.
        _blServicesView->Enable();
        // Expand the services view.
        _blServicesView->Expand();

        // Enable the connect button.
        ui->connectbtn->setEnabled(true);
    }
    // Update the services list.
    _blServicesView->update();
}

void MainWindow::updateChart()
{
    // Get the current size of the data.
    int seriesSize = _llhpsDataProcessor->GetSeries()->count();
    // Replace series with new series, this is more optimized for performance, than clear and append.
    series->replace(*_llhpsDataProcessor->GetSeries());
    // Update range.
    chart->axisX()->setRange(0, seriesSize);
}

void MainWindow::serviceNotification(QLowEnergyCharacteristic, QByteArray val)
{
    // The potentiometer value is 16 bit wide.
    uint16_t pot = 0;
    // Set the most significant byte.
    pot = (uint8_t)val.at(0);
    // Shift that 8 bits to the left.
    pot <<= 8;
    // Set the least significant byte.
    pot |= (uint8_t)val.at(1);

    // Check if the thread is currently processing the chart data.
    if (!_llhpsDataProcessor->isRunning())
    {
        // Add point to chart.
        _llhpsDataProcessor->AddPoint(pot);
        // Start thread.
        _llhpsDataProcessor->start();
    }
}

void MainWindow::characteristicsUpdated()
{
    // Get currently connected service.
    ServiceInfo* currentService = _bluetooth->getCurrentService();
    // Get list of characteristics.
    QList<QObject*>* chars = _bluetooth->getCharacteristics();

    // Check if there are any characteristics.
    if (chars->size() > 0)
    {
        // Get the first characteristic, we assume that there is always only one characteristic.
        CharacteristicInfo * characteristic = ((CharacteristicInfo*)chars->at(0));
        // Get the descriptor from the characteristic.
        QLowEnergyDescriptor descriptor = characteristic->m_characteristic.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        // Check if the descriptor is valid.
        if (!descriptor.isValid())
        {
            // Print error message.
            qDebug() << "Characteristic: " << characteristic->getName() << " has an invalid descriptor.";
            // Return.
            return;
        }
        // Update the bluetooth status to connected to a service.
        _bluetoothStatus = BLUETOOTH_STATUS_CONNECTED_SERVICE;
        // Set the service status button to status OK.
        _blServicesView->SetStatus(StatusButton::STATUS_OK);
        // Print confirmation to the console.
        _blServicesView->SetTitle("Connected to "+currentService->getName());
        // Disable the services button.
        _blServicesView->Disable();

        // Disable the connect button.
        ui->connectbtn->setEnabled(true);
        // Update the connect button text.
        ui->connectbtn->setText("Disconnect");
        // Start the timer to refresh the chart.
        _timer->start();
        // Clear the screen.
        _lcdModel->ClearScreen();

        // Connect the characteristic signal to a slot.
        connect(currentService->m_service, &QLowEnergyService::characteristicChanged, this, &MainWindow::serviceNotification);
        // Enable notifications.
        currentService->m_service->writeDescriptor(descriptor, QByteArray::fromHex("0100"));
    }
}
