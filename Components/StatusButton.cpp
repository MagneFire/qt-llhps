#include "StatusButton.h"

StatusButton::StatusButton(const QString &text, QPushButton *parent) : QPushButton(parent)
{
    setStyleSheet("QPushButton { border: none; text-align: left}");
    setLayoutDirection(Qt::RightToLeft);

    setText(text);

    iconOK.addPixmap(QPixmap(":/icons/ok.png"));
    iconWarning.addPixmap(QPixmap(":/icons/warning.png"));
    iconError.addPixmap(QPixmap(":/icons/error.png"));

    connect(this, &QPushButton::clicked, this, &StatusButton::clicked);
    movieLoading.setFileName(":/icons/loader.gif");
    connect(&movieLoading, &QMovie::frameChanged, this, &StatusButton::updateLoaderIcon);
}

bool StatusButton::event(QEvent * e)
{
   if (e->type() == QEvent::Paint)
      return QPushButton::event(e);
   else if (e->type() == QEvent::MouseButtonRelease)
       emit QPushButton::clicked();
   return true;
}

void StatusButton::setStatus(const StatusButtonStatuses & status)
{
    movieLoading.stop();
    switch(status)
    {
    case STATUS_NO_STATUS:
        setIcon(QIcon());
        break;
    case STATUS_OK:
        setIcon(iconOK);
        break;
    case STATUS_LOADING:
        movieLoading.start();
        break;
    case STATUS_WARNING:
        setIcon(iconWarning);
        break;
    case STATUS_ERROR:
        setIcon(iconError);
        break;
    }
}

void StatusButton::updateLoaderIcon()
{
    setIcon(QIcon(movieLoading.currentPixmap()));
}
