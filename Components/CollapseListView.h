#ifndef COLLAPSELISTVIEW_H
#define COLLAPSELISTVIEW_H

#include <QListView>
#include <QModelIndex>
#include "./CollapsePanel.h"
#include "./StatusButton.h"
#include "../Bluetooth/deviceinfo.h"

class CollapseListView : public QWidget
{
    Q_OBJECT
private:
    /// The CollapseListView layout.
    QVBoxLayout mainLayout;

    CollapsePanel * _collapsePanel;
    StatusButton * _statusButton;
    QListView * _listView;
public:
    explicit CollapseListView(const QString &title = "", QWidget * parent = 0);

    /**
     * @brief Set the title for the status button.
     * @param title The title.
     */
    inline void SetTitle(const QString &title) {_statusButton->setText(title);}
    /**
     * @brief Set the button status.
     * @param status The status to use.
     */
    inline void SetStatus(const StatusButton::StatusButtonStatuses & status) {_statusButton->setStatus(status);}

    /**
     * @brief Set the item model for the ListView.
     * @param model The model to be used.
     */
    inline void SetModel(QAbstractItemModel* model) {_listView->setModel(model);}

    /**
     * @brief Get the current index of the selected item.
     * @return The current index.
     */
    inline QModelIndex currentIndex() {return _listView->currentIndex();}

    /**
     * @brief Enable/Disable the CollapseListView.
     * @param enable Enables/Disables the CollapseListView.
     */
    void SetEnabled(bool enable);
    /**
     * @brief Disable the CollapseListView.
     */
    inline void Disable() {SetEnabled(false);}
    /**
     * @brief Enable the CollapseListView.
     */
    inline void Enable() {SetEnabled(true);}

    /**
     * @brief Collapse the collapse panel.
     */
    inline void Collapse() {_collapsePanel->collapse();}
    /**
     * @brief Expand the collapse panel.
     */
    inline void Expand() {_collapsePanel->expand();}
signals:
    /**
     * @brief ListView item double click signal.
     *
     * This signal is emited when an ListView item is clicked.
     */
    void ItemClicked(QModelIndex);
    /**
     * @brief ListView item double double click signal.
     *
     * This signal is emited when an ListView item is double clicked.
     */
    void ItemDoubleClicked(QModelIndex);
    /**
     * @brief Status button clicked signal.
     *
     * This signal is emited when the status button is clicked.
     */
    void StatusClicked(bool);
public slots:
    /**
     * @brief Update the listview.
     *
     * This function forcefully reloads the listview.
     */
    void update();
};

#endif // COLLAPSELISTVIEW_H
