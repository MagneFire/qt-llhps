#include "CollapseListView.h"

#include <QLabel>
#include <QMovie>

CollapseListView::CollapseListView(const QString &title, QWidget * parent)
    : QWidget(parent)
{
    QVBoxLayout * layoutDevices = new QVBoxLayout();
    // Set margins to zero.
    layoutDevices->setContentsMargins(0, 0, 0, 0);
    _statusButton = new StatusButton(title);
    _listView = new QListView();
    _listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    layoutDevices->addWidget(_listView);
    _collapsePanel = new CollapsePanel();
    _collapsePanel->setContentLayout(*layoutDevices);
    mainLayout.insertWidget(0, _statusButton);
    mainLayout.insertWidget(1, _collapsePanel);

    // Set margins to zero.
    mainLayout.setContentsMargins(0, 0, 0, 0);
    // Set layout.
    setLayout(&mainLayout);

    connect(_listView, &QListView::clicked, this, &CollapseListView::ItemClicked);
    connect(_listView, &QListView::doubleClicked, this, &CollapseListView::ItemDoubleClicked);
    connect(_statusButton, &QPushButton::clicked, this, &CollapseListView::StatusClicked);
}

void CollapseListView::update()
{
    // Get the current index of the selected item.
    QModelIndex index = _listView->currentIndex();
    // Reset view, this updates the data shown in the list.
    _listView->reset();
    // Select the item to the previously selected index.
    _listView->setCurrentIndex(index);
    // Repaint the listview.
    _listView->repaint();
    // Repaint the status button.
    _statusButton->repaint();
}

void CollapseListView::SetEnabled(bool enable)
{
    // Enable/Disable status button.
    _statusButton->setEnabled(enable);
    // Enable/Disable listview.
    _listView->setEnabled(enable);
    // Enable/Disable collapse panel.
    _collapsePanel->setEnabled(enable);
}
