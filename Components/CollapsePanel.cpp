#include "CollapsePanel.h"

#include <QLabel>
#include <QMovie>

CollapsePanel::CollapsePanel(const int animationDuration, QWidget *parent) : QWidget(parent), animationDuration(animationDuration) {
    /*toggleButton.setStyleSheet("QToolButton { border: none; }");
    toggleButton.setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    toggleButton.setArrowType(Qt::ArrowType::RightArrow);
    toggleButton.setText(title);
    toggleButton.setCheckable(true);
    toggleButton.setChecked(false);*/

    headerLine.setFrameShape(QFrame::HLine);
    headerLine.setFrameShadow(QFrame::Sunken);
    headerLine.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);

    contentArea.setStyleSheet("QScrollArea { border: none; }");
    contentArea.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    // start out collapsed
    contentArea.setMaximumHeight(0);
    contentArea.setMinimumHeight(0);
    // let the entire widget grow and shrink with its content
    toggleAnimation.addAnimation(new QPropertyAnimation(this, "minimumHeight"));
    toggleAnimation.addAnimation(new QPropertyAnimation(this, "maximumHeight"));
    toggleAnimation.addAnimation(new QPropertyAnimation(&contentArea, "maximumHeight"));
    // don't waste space
    //mainLayout.setVerticalSpacing(0);
    mainLayout.setContentsMargins(0, 0, 0, 0);
    //int row = 0;
    //mainLayout.addWidget(&toggleButton, row, 0, 1, 1, Qt::AlignLeft);
    QLabel * lbl = new QLabel();
    //QPixmap mypix (":/icons/loading.png");
    //lbl->setPixmap(mypix);

    QMovie *movie = new QMovie(":/icons/loader.gif");
    lbl->setMovie(movie);
    movie->start();

    //mainLayout.addWidget(lbl, row, 2, 1, 1, Qt::AlignLeft);
    //mainLayout.addWidget(&headerLine, row++, 1, 1, 1);
    mainLayout.addWidget(&contentArea);//, row, 0, 1, 3);
    setLayout(&mainLayout);
}

void CollapsePanel::setContentLayout(QLayout & contentLayout) {
    delete contentArea.layout();
    contentArea.setLayout(&contentLayout);
    const auto collapsedHeight = sizeHint().height() - contentArea.maximumHeight();
    auto contentHeight = contentLayout.sizeHint().height();
    for (int i = 0; i < toggleAnimation.animationCount() - 1; ++i) {
        QPropertyAnimation * spoilerAnimation = static_cast<QPropertyAnimation *>(toggleAnimation.animationAt(i));
        spoilerAnimation->setDuration(animationDuration);
        spoilerAnimation->setStartValue(collapsedHeight);
        spoilerAnimation->setEndValue(collapsedHeight + contentHeight);
    }
    contentAnimation = static_cast<QPropertyAnimation *>(toggleAnimation.animationAt(toggleAnimation.animationCount() - 1));
    contentAnimation->setDuration(animationDuration);
    //contentAnimation->setEasingCurve(QEasingCurve::Linear);
    contentAnimation->setEasingCurve(QEasingCurve::InExpo);
    contentAnimation->setStartValue(0);
    contentAnimation->setEndValue(contentHeight);
}


void CollapsePanel::collapse()
{
    // Sanity check, don't collapse, if already collapsed.
    if (collapseState == COLLAPSE_STATE_COLLAPSED) return;
    toggleAnimation.setDirection(QAbstractAnimation::Backward);
    toggleAnimation.start();
    collapseState = COLLAPSE_STATE_COLLAPSED;
}

void CollapsePanel::expand()
{
    // Sanity check, don't expand, if already expanded.
    if (collapseState == COLLAPSE_STATE_EXPANDED) return;
    //toggleButton.setArrowType(Qt::ArrowType::DownArrow);
    toggleAnimation.setDirection(QAbstractAnimation::Forward);
    toggleAnimation.start();
    collapseState = COLLAPSE_STATE_EXPANDED;
}


void CollapsePanel::toggle()
{
    if (collapseState == COLLAPSE_STATE_COLLAPSED)
        expand();
    else
        collapse();
}
