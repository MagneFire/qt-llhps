#ifndef COLLAPSEPANEL_H
#define COLLAPSEPANEL_H

#include <QFrame>
#include <QGridLayout>
#include <QParallelAnimationGroup>
#include <QScrollArea>
#include <QToolButton>
#include <QWidget>
#include <QPropertyAnimation>

class CollapsePanel : public QWidget
{
    Q_OBJECT
public:

    typedef enum _CollapsePanelState
    {
        COLLAPSE_STATE_COLLAPSED,   ///<
        COLLAPSE_STATE_COLLAPSING,  ///<
        COLLAPSE_STATE_EXPANDED,    ///<
        COLLAPSE_STATE_EXPANDING    ///<
    } CollapsePanelState;

    QVBoxLayout mainLayout;
    QFrame headerLine;
    QParallelAnimationGroup toggleAnimation;
    QScrollArea contentArea;
    QPropertyAnimation * contentAnimation;
    int animationDuration{300};
    CollapsePanelState collapseState;
public:
    explicit CollapsePanel(const int animationDuration = 300, QWidget *parent = 0);
    void setContentLayout(QLayout & contentLayout);

    void collapse();
    void expand();
    void toggle();
};

#endif // COLLAPSEPANEL_H
