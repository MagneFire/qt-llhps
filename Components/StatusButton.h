#ifndef STATUSLABEL_H
#define STATUSLABEL_H

#include <QDebug>

#include <QPushButton>
#include <QMovie>

class StatusButton : public QPushButton
{
    Q_OBJECT
private:
    QIcon iconOK;
    QIcon iconWarning;
    QIcon iconError;
    QMovie movieLoading;
public:

    typedef enum _StatusButtonStatuses
    {
        STATUS_NO_STATUS,
        STATUS_OK,
        STATUS_LOADING,
        STATUS_WARNING,
        STATUS_ERROR
    } StatusButtonStatuses;

    explicit StatusButton(const QString &text, QPushButton *parent = 0);

    inline void setText(const QString &text) {QPushButton::setText(text);}

    void setStatus(const StatusButtonStatuses & status);

    bool event(QEvent * e);

signals:
    void clicked(bool);

public slots:
    void updateLoaderIcon();
};

#endif // STATUSLABEL_H
