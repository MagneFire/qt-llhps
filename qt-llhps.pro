#-------------------------------------------------
#
# Project created by QtCreator 2016-12-21T10:04:17
#
#-------------------------------------------------

QT       += core gui charts bluetooth

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt-llhps
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
        Bluetooth/device.cpp \
        Bluetooth/deviceinfo.cpp \
        Bluetooth/characteristicinfo.cpp \
        Bluetooth/serviceinfo.cpp \
        Models/DeviceInfoListModel.cpp \
        Models/ServiceInfoListModel.cpp \
        Models/CharacteristicInfoModel.cpp \
        Components/CollapsePanel.cpp \
        Components/StatusButton.cpp \
        Components/CollapseListView.cpp \
        LLHPSDataProcessor.cpp \
        LCDDevice.cpp

HEADERS  += mainwindow.h \
        Bluetooth/device.h \
        Bluetooth/deviceinfo.h \
        Bluetooth/characteristicinfo.h \
        Bluetooth/serviceinfo.h \
        Models/DeviceInfoListModel.h \
        Models/ServiceInfoListModel.h \
        Models/CharacteristicInfoModel.h \
        Components/CollapsePanel.h \
        Components/StatusButton.h \
        Components/CollapseListView.h \
        LLHPSDataProcessor.h \
        LCDDevice.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
