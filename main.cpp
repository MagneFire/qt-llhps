#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // The Main application.
    MainWindow w;
    // Show main window.
    w.show();

    return a.exec();
}
