#include "LCDDevice.h"

void LCDDevice::Write(const QString &text)
{
    // Create a QFile.
    QFile file(LCD_DEV);
    // Open the device for writing only.
    if (file.open(QIODevice::WriteOnly))
    {
        // Create a QTextStream with a reference to the QFile.
        QTextStream stream(&file);
        // Write text to stream.
        stream << text;
    }
}

void LCDDevice::ClearScreen()
{
    // Create a buffer with the size of the LCD.
    char buffer[LCD_WIDTH * LCD_HEIGHT];
    // Fill the buffer with spaces.
    memset(buffer, ' ', LCD_WIDTH * LCD_HEIGHT);
    // Write buffer to LCD.
    Write(buffer);
}
