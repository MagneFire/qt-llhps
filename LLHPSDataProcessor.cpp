#include "LLHPSDataProcessor.h"

void LLHPSDataProcessor::run()
{
    // Set the first part of the string for the LCD.
    QString lcdString = "Pot value: ";
    // Convert pot value to string and append to lcd string.
    lcdString += QString::number(chartVal);
    // Clear everything after this number for the first row.
    while(lcdString.size() < 16)
        lcdString += " ";

    // Write string to LCD.
    _lcdModel->Write(lcdString);

    // Check if points should be added or shifted.
    if (_serie2.count() < CHART_MAX_VALUES)
    {
        // Get the current size of the series.
        int seriesSize = _serie2.count();
        qreal x = 0;
        if (seriesSize > 0)
        // Get the last x element and add one.
        x = _serie2.at(seriesSize - 1).x() + 1;
        // Add new point.
        _serie2.append(QPointF(x, chartVal));
    }
    else
    {
        // Loop through all the chart values.
        for (uint8_t i = 1; i < CHART_MAX_VALUES; i++)
            //  Move chart value one byte to the left.
            _serie2.replace(i-1, QPointF(i-1, _serie2.at(i).y()));

        // Replace the last chart value with the new potmeter value.
        _serie2.replace(CHART_MAX_VALUES-1, QPointF(CHART_MAX_VALUES-1, chartVal));
    }
}
