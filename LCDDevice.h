#ifndef __MODELS_LCDDEVICE_H_
#define __MODELS_LCDDEVICE_H_

#include <QObject>
#include <QFile>
#include <QTextStream>

/// The location of the LCD device.
#define LCD_DEV "/dev/lcd"
/// The height of the LCD.
#define LCD_HEIGHT 2
// The width of the LCD.
#define LCD_WIDTH 16

/**
 * @brief The LCDDevice class.
 *
 * This class is responsible for writing text to an external LCD device.
 */
class LCDDevice : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief The LCDDevice constructur.
     * @param parent The parent.
     */
    inline LCDDevice(QObject *parent = 0) : QObject(parent) {}
    /**
     * @brief Write text to the LCD.
     * @param text The text to be written.
     */
    void Write(const QString &text);
    /**
     * @brief Clear the screen.
     *
     * This function sends enough spaces so that the screen will appear as clear.
     */
    void ClearScreen();
};


#endif // __MODELS_LCDDEVICE_H_
